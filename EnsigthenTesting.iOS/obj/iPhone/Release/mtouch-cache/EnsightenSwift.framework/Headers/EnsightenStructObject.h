//
//  NSTNStructObject.h
//  Ensighten
//
//  Created by Zack Ulrich on 10/9/13.
//
//

#import <Foundation/Foundation.h>

typedef enum OFFSET_TYPE {
    OFFSET_2,
    OFFSET_4,
    OFFSET_8
}STRUCT_OFFSET;

@interface ESWStructObject : NSObject


@property (nonatomic, retain) NSString *structName;

-(void)addObjectToStruct:(id)object;
-(id)getStructObjectAtIndex:(int)index;
-(id)getStructValues;
- (BOOL)isEqual:(id)other;

-(id)initWithName:(NSString *)name;
-(NSString *)description;

+(ESWStructObject *)setupStructWithEncoding:(NSString *)encoding pointer:(char *)ptr;

@end
