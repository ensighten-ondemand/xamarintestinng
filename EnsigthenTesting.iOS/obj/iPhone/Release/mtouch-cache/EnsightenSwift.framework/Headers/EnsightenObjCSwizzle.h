//
//  EnsightenObjCSwizzle.h
//  EnsightenSwift
//
//  Created by Miles Alden on 1/16/17.
//  Copyright © 2017 Ensighten. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface EnsightenObjCSwizzle : NSObject

+ (EnsightenObjCSwizzle *)sharedInstance;
    
+(void)swizzleMethods;
    
+ (void)swizzleMethodImpFromClass:(NSString*)source toClass:(NSString*)dest forSelector:(NSString*)selector withPrefix:(NSString*)prefix;
+ (void)addEntryToLookupClass:(NSString *)_cl entry:(NSDictionary *)d key:(NSString *)key;
+ (NSDictionary *)entryForClass:(NSString *)_cl andSel:(NSString *)_sel;

@end
