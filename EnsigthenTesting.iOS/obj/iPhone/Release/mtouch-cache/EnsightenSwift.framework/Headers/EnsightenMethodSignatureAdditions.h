//
//  NSMethodSignature+ESWMethodSignatureAdditions.h
//  EnsightenSwift
//
//  Created by Miles Alden on 9/9/16.
//  Copyright © 2017 Ensighten. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMethodSignature (ESWMethodSignatureAdditions)


-(BOOL)ESWIsReturnStruct;

@end
