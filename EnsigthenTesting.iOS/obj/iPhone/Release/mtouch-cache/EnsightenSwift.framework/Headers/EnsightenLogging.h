//
//  EnsightenLogging.h
//  EnsightenSwift
//
//  Created by Miles Alden on 9/1/16.
//  Copyright © 2017 Ensighten. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface EnsightenLogging : NSObject

void ESWSystemLog (NSString * fmt);
void ESWLog (int level, NSString * message);

@end
