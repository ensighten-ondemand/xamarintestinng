#ifndef EnsightenReflection_h
#define EnsightenReflection_h

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <string.h>

/**
 * The maximum number of parameters that are supported by Ensighten's Swift framework.
 * Some data structures in swift may take multiple reflections parameters, such as the 
 * Swift String which has a char pointer, the count, and the owner as three parameters.
 */
static const int MAX_PARAMS = 64;

/**
 * The various error codes.
 */
static const int ERROR_CODE_SUCCESS = 0;
static const int ERROR_CODE_METHOD_NOT_FOUND = 1;
static const int ERROR_CODE_NO_PARAMETERS;

typedef struct objc_object *id;
typedef struct returnType { void* first; void* second; } returnType;

static const returnType NULL_VALUE = (returnType){ 0 };

/**
 * Headers for adding a parameter to the list of params
 */
bool _addBoolParam(bool param);
bool _addClassParam(id param);
bool _addIntParam(int param);
bool _addUIntParam(unsigned int param);
bool _addStringParam(const char* param, int count);
bool _addParam(void* param);
void _clearParams();

/**
 * Headers 
 */
int _getErrorCode();
bool _isReturnValueNil();

/**
 * Headers for calling methods with specific return types
 */
bool _callMethodBool(const char* methodSignature);
id _callMethodClass(const char* methodSignature);
double _callMethodDouble(const char* methodSignature);
float _callMethodFloat(const char* methodSignature);
int _callMethodInt(const char* methodSignature);
unsigned int _callMethodUInt(const char* methodSignature);
const char* _callMethodString(const char* methodSignature);
returnType _callMethod(const char* methodSignature);

/**
 * Headers for calling static methods with specific return types
 */
bool _callStaticMethodBool(const char* classSignature, const char* methodSignature);
id _callStaticMethodClass(const char* classSignature, const char* methodSignature);
double _callStaticMethodDouble(const char* classSignature, const char* methodSignature);
float _callStaticMethodFloat(const char* classSignature, const char* methodSignature);
int _callStaticMethodInt(const char* classSignature, const char* methodSignature);
unsigned int _callStaticMethodUInt(const char* classSignature, const char* methodSignature);
const char* _callStaticMethodString(const char* classSignature, const char* methodSignature);
returnType _callStaticMethod(const char* classSignature, const char* methodSignature);

/**
 * Headers for calling methods with a specific number of parameters
 */
returnType _callMethodOneParameter(const char* methodSignature);
returnType _callMethodTwoParameters(const char* methodSignature);
returnType _callMethodThreeParameters(const char* methodSignature);
returnType _callMethodFourParameters(const char* methodSignature);
returnType _callMethodFiveParameters(const char* methodSignature);
returnType _callMethodSixParameters(const char* methodSignature);
returnType _callMethodSevenParameters(const char* methodSignature);
returnType _callMethodEightParameters(const char* methodSignature);
returnType _callMethodNineParameters(const char* methodSignature);

/**
 * Headers for calling methods with a double return value with a specific number of parameters
 */
double _callMethodNoParametersDouble(const char* methodSignature);
double _callMethodOneParameterDouble(const char* methodSignature);
double _callMethodTwoParametersDouble(const char* methodSignature);
double _callMethodThreeParametersDouble(const char* methodSignature);
double _callMethodFourParametersDouble(const char* methodSignature);
double _callMethodFiveParametersDouble(const char* methodSignature);
double _callMethodSixParametersDouble(const char* methodSignature);
double _callMethodSevenParametersDouble(const char* methodSignature);
double _callMethodEightParametersDouble(const char* methodSignature);
double _callMethodNineParametersDouble(const char* methodSignature);

/**
 * Headers for calling methods with a float return value with a specific number of parameters
 */
float _callMethodNoParametersFloat(const char* methodSignature);
float _callMethodOneParameterFloat(const char* methodSignature);
float _callMethodTwoParametersFloat(const char* methodSignature);
float _callMethodThreeParametersFloat(const char* methodSignature);
float _callMethodFourParametersFloat(const char* methodSignature);
float _callMethodFiveParametersFloat(const char* methodSignature);
float _callMethodSixParametersFloat(const char* methodSignature);
float _callMethodSevenParametersFloat(const char* methodSignature);
float _callMethodEightParametersFloat(const char* methodSignature);
float _callMethodNineParametersFloat(const char* methodSignature);

/**
 * Headers for getting properties
 */
void _addParamFromString(const char* classSignature);

#endif /* EnsightenReflection_h */
