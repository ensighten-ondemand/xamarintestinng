﻿using System;
using FormsPlugin.Ensighten;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EnsigthenTesting
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            Ensighten.Bootstrap("mobile_training", "androidDemo", "swiftDemo");
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
